# Project

The viewer is available at: https://hanzi-viewer.netlify.app

This project allows you to:
- See all the unique 汉字 you know from a list of words
- See all the words related to each 汉字
- To go through random 汉字 to practice recognition

> Disclaimer: This project has been developed quickly, it does not follow the best practices and could be heavily improved.
